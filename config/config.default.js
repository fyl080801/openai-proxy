"use strict"

/* eslint valid-jsdoc: "off" */

const { v4 } = require("uuid")

module.exports = (appInfo) => {
  const config = (exports = {})

  config.keys = appInfo.name + "_18462223343376_5216"

  config.middleware = []

  config.cluster = {
    listen: {
      port: +(process.env.PORT || 3001),
      hostname: "0.0.0.0"
    }
  }

  config.httpProxy = {
    "/api": {
      logLevel: "info",
      target: "https://api.openai.com",
      changeOrigin: true,
      pathRewrite: {
        "^/api": ""
      }
    }
  }

  return {
    ...config
  }
}
